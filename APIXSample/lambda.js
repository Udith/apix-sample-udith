let AWS = require('aws-sdk');
const cognito_idp = new AWS.CognitoIdentityServiceProvider();
let Swagger = require('swagger-client');

exports.handler = function (event, context, callback) {

    Swagger.http({
        url: `https://services.apixplatform.com/api-sandbox/application/token`,
        method: 'post',
        query: {},
        headers: { "Accept": "*/*", "Content-Type": "application/json" },
        body: {
            "id": 123
        }
    }).then((response) => {
        console.log("Received response", response);
        callback(null, response);
    }).catch((err) => {
        console.log("Error occurred", err);
        callback(err);
    });
    
    cognito_idp.listUsers({
        UserPoolId: "us-east-1_uVXTQInep",
        Limit: "10"
    }, function (error, data) {
        if (error) {
            // implement error handling logic here
            throw error;
        }
        // your logic goes within this block
    });


}